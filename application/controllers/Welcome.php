<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Welcome extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();

  // load helper url dan model

  $this->load->model('welcome_model');

  $this->load->helper('url');

 }



 public function index()

 {

  // panggil fungsi model

  $result ['data'] = $this->welcome_model->get_data();

  // menampilkan view welcome_message.php dengan parameter $result data siswa

  $this->load->view('welcome_message', $result);

 }



 public function tambah(){

  // menampilkan view tambah_kamar.php

  $this->load->view('tambah_kamar');

 }



 public function save(){

  // menangkap data POST dari form tambah_kamar.php

  $datapos = $this->input->post();

  // memanggil model untuk simpan data dengan parameter data post

  $data = $this->welcome_model->save_data($datapos);



  redirect( base_url() . 'index.php/welcome');

 }



 function ubah(){

  // mengambil value segment 3 dari url, id_siswa

  $id = $this->uri->segment(3);

  $data = $this->welcome_model->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('ubah_kamar', $result);

 }



 public function update(){

  $datapos = $this->input->post();

  $data = $this->welcome_model->update_data($datapos);



  // memanggil controller index untuk kembali mengampilkan data table

  redirect( base_url() . 'index.php/welcome');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->welcome_model->hapus_data($id);

  

  // memanggil controller index untuk kembali mengampilkan data table

  redirect( base_url() . 'index.php/welcome');

 }



}
