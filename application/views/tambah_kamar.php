<!DOCTYPE html>

<html>

<head>

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>



<div class="container">

   <h3>Tambah Data Kamar</h3>

  <div class="table-responsive">

  <table class="table">

    <thead>

      <tr>

        <th></th>

        <th></th>

      </tr>

    </thead>

    <tbody>

      <form action="<?php echo base_url().'index.php/welcome/save';?>" method="post">

      <tr>

        <td width="100">Nomor kamar</td>

        <td width="300">

          <input type="text" class="form-control" name="nomor_kamar" placeholder="Nomor kamar">

        </td>

      </tr>



      <tr>

        <td width="100">Tipe kamar</td>

        <td width="300">

         <select class="form-control" name="id_kamar_tipe">
          <option>--Pilih--</option>
                <option value="STANDART">STANDART </option>
                <option value="SUPERIOR">SUPERIOR </option>
                <option value="SUITE">SUITE </option>
              

        </td>

      </tr>



      <tr>

        <td width="100">Max dewasa</td>

        <td width="300">

          <select class="form-control" name="max_dewasa">
          <option>--Pilih--</option>
                <option value="1">1 Orang </option>
                <option value="2">2 Orang </option>
                <option value="3">3 Orang </option>
                <option value="4">4 Orang </option>
                <option value="5">5 Orang </option>

        </td>

      </tr>
      <tr>

        <td width="100">Max anak</td>

        <td width="300">

          <select class="form-control" name="max_anak">
          <option>--Pilih--</option>
                <option value="1">1 Orang </option>
                <option value="2">2 Orang </option>
                <option value="3">3 Orang </option>
                <option value="4">4 Orang </option>
                <option value="5">5 Orang </option>

        </td>

      </tr>

      <tr>

        <td width="100">Status kamar</td>

        <td width="300">

          <select class="form-control" name="status_kamar">
          <option>--Pilih--</option>
                <option value="TERSEDIA">TERSEDIA</option>
                <option value="TERPAKAI">TERPAKAI</option>
                <option value="KOTOR">KOTOR</option>
           

        </td>

      </tr>



      <tr>

        <td colspan="2">

          <a href="<?php echo base_url().'index.php/welcome/';?>" class="btn btn-info">Batal</a>

          <button type="submit" class="btn btn-primary">Simpan</button>

        </td>

      </tr>



    </form>

    </tbody>

  </table>

  </div>

</div>


</body>

</html>