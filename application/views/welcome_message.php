<!DOCTYPE html>

<html>

<head>

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <meta charset="utf-8">
<title>Dashboard - Aplikasi Reservasi hotel</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?php echo base_url(); ?>___/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>___/css/style.css" rel="stylesheet">

</head>

<body>
  <div class="wrapper">
      <nav class="navbar navbar-default">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" >Kamar</a>
        
        </div>
      </nav>



<div class="container">

   <h3>Kamar</h3>

  <a href="<?php echo base_url().'index.php/welcome/tambah';?>" class="btn btn-primary">Tambah</a>

  <div class="table-responsive">

  <table class="table">

    <thead>

      <tr>

        <th>#</th>

        <th>Nomor kamar</th>

        <th>Tipe kamar</th>

        <th>Max. dewasa</th>

        <th>Max. anak</th>

        <th>Status</th>

    <th>



    </th>

      </tr>

    </thead>

    <tbody>



    <?php

    $i = 1;

    foreach ($data as $key => $value) {

     ?>

     <tr>

      <td><?php echo $i;?></td>

          <td><?php echo $value->nomor_kamar;?></td>

          <td><?php echo $value->id_kamar_tipe;?></td>

          <td><?php echo $value->max_dewasa;?> Orang</td>

           <td><?php echo $value->max_anak;?> Orang</td>

           <td><?php echo $value->status_kamar;?></td>

      <td>

      

        <!-- <button type="button" class="btn btn-primary" value="Ubah"></button> -->

        <a href="<?php echo base_url().'index.php/welcome/ubah/'.$value->id_kamar;?>" class="btn btn-info">Ubah</a>

        <a href="<?php echo base_url().'index.php/welcome/hapus/'.$value->id_kamar;?>" class="btn btn-danger">Hapus</a>
        
                 

      </td>

     </tr>

     <?php

     $i++;

    }

    ?>

    </tbody>

  </table>

  </div>

</div>



</body>

</html>