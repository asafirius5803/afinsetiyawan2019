<!DOCTYPE html>  
<html lang="en">  
<head>  
   <title>Login</title>  
   <meta charset="utf-8">  
   <meta http-equiv="X-UA-Compatible" content="IE=edge">  
   <meta name="viewport" content="width=device-width, initial-scale=1">  
   <!-- Latest compiled and minified CSS -->  
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >  
   <style type="text/css">  
    .form-box{  
     max-width: 500px;  
    position: relative;  
     margin: 5% auto;  
  </style>  
  </head>  
  <body>  
  <div class="wrapper">  
   <div class="container">  
     <div class="row">  
           <div class="form-box">  
     <div class="panel panel-primary">  
        <div class="panel-heading text-center">  
        <h3>Login</h3>  
        </div>  
        <div class="panel-body">  
         <div class="row">  
           <div class="col-sm-12">  
           </div>  
         </div>  
         <form action="<?php echo base_url('index.php/halaman_admin');?>" method="post">  
          <div class="form-group">  
                  <label class="control-label" for="pswd">Username</label>  
                    <div>  
                     <input type="text" class="form-control" id="Username" name="Username" placeholder="Username" required="">  
                          
                    </div>  
                </div>  
                <div class="form-group">  
                 <label class="control-label" for="pswd">Password</label>  
                    <div>  
                      <input type="password" class="form-control" id="pswd" name="password" placeholder="Password" required="">  
                     
                                          </div>  
                </div>  
                <div class="form-group">   
                  <div class="row">  
                    <div class="col-sm-offset-5 col-sm-3 btn-submit">  
                      <button type="submit" class="btn btn-success">Login</button>  
                                          </div>  
                  </div>  
                </div>  
         </form>  
        </div>  
        <div class="panel-footer text-center">  
         <p><a href="<?php echo base_url() ?>">REGISTER</a></p>   
        </div>  
       </div>  
      </div>  
     </div>  
    </div>  
   </div>  
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
  </body>  
 </html>  